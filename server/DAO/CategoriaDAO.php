<?php
include_once './conection.php';

/**
* Classe responsavel por querys da tabela de movimentacao
*/
class CategoriaDAO{


  function __construct()  {
  }

  public static function consultarCategorias(){
    try {
      $connection = OpenCon();
      $query = "select * from categoria order by descricao";

      mysqli_set_charset($connection, 'utf8');
      $result = mysqli_query($connection, $query);

      $categoria = array();

      while ($item = mysqli_fetch_assoc($result)) {
        array_push($categoria, $item);
      }
      return $categoria;
    } catch (Exception $e) {
      return null;
    }finally{
      CloseCon($connection);
    }
  }


}

?>
