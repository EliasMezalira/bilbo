<?php
include_once './conection.php';

/**
* Classe responsavel por querys da tabela de movimentacao
*/
class MovimentacaoDAO{


  function __construct()
  {
    // code...
  }

  public function consultarMovimentacoes(){
    try {
      $connection = OpenCon();
      $query = "select * from movimentacao order by data";

      mysqli_set_charset($connection, 'utf8');
      $result = mysqli_query($connection, $query);

      $categoria = array();

      while ($item = mysqli_fetch_assoc($result)) {
        array_push($categoria, $item);
      }
      return $categoria;
    } catch (Exception $e) {
      return null;
    }finally{
      CloseCon($connection);
    }
  }

  public static function InserirMovimentacao($tipo,$categoria, $data, $descricao, $valor){
    try {
      $connection = OpenCon();
      $query = "INSERT INTO `financas_pessoais`.`movimentacao` (`tipo`, `categoria`, `data`, `valor`, `descricao`) VALUES ($tipo, $categoria, '$data', $valor, $descricao);";

      // die($query);

      mysqli_set_charset($connection, 'utf8');
      if(mysqli_query($connection, $query)){
        return "foi";
      }else {

        return mysqli_error($connection);
      }

    } catch (Exception $e) {
      return null;
    }finally{
      CloseCon($connection);
    }
  }


}

?>
