<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once 'bootstrap.php';
require_once './DAO/CategoriaDAO.php';
require_once './DAO/movimentacaoDAO.php';

$app->get('/categoria/todas', function (Request $request, Response $response) {
  $resultado = CategoriaDAO::consultarCategorias();

  $return = $response->withHeader('Content-type','application/json')
  ->withHeader('Access-Control-Allow-Origin', '*')
  ->write(json_encode($resultado, JSON_UNESCAPED_UNICODE));

  return $return;
});

$app->get('/movimentacao/todas', function (Request $request, Response $response) {
  $resultado = MovimentacaoDAO::consultarMovimentacoes();

  $return = $response->withHeader('Content-type','application/json')
  ->withHeader('Access-Control-Allow-Origin', '*')
  ->write(json_encode($resultado, JSON_UNESCAPED_UNICODE));

  return $return;
});

$app->post('/movimentacao/inserir/{tipo}/{categoria}/{data}/{descricao}/{valor}', function (Request $request, Response $response, array $args) {
  $tipo = $args['tipo'];
  $categoria = $args['categoria'];
  $data = $args['data'];
  $descricao = $args['descricao'];
  $valor = $args['valor'];


  $retono = MovimentacaoDAO::InserirMovimentacao($tipo,$categoria, $data, $descricao, $valor);
  $retono = "{resultado:". $retono."}";

  $return = $response->withHeader('Content-type','application/json')
  ->withHeader('Access-Control-Allow-Origin', '*')
  ->write(json_encode($retono, JSON_UNESCAPED_UNICODE));

  return $response;
});

/**
*Retorna json contendo as categorias de registro
*/
// $app->get('/retorna-categorias', functio($request, $response){
//   $response->whitHeader('Content-type', 'application-json');
//
//   return $response;
// });

$app->run();
